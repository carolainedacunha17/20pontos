#!/bin/bash

d=$(date +%Y-%m-%D-%h)

x=/tmp/${d}

mkdir $x

cp * $x

zip -r a.zip $x
rm -rf $x
